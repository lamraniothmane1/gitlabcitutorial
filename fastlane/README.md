fastlane documentation
================
# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```
xcode-select --install
```

Install _fastlane_ using
```
[sudo] gem install fastlane -NV
```
or alternatively using `brew cask install fastlane`

# Available Actions
## Android
### android unit_tests
```
fastlane android unit_tests
```
Runs all the unit tests for the project
### android ui_tests_on_emulator
```
fastlane android ui_tests_on_emulator
```
Runs all the UI tests on the Emulator
### android ui_tests_on_firebase_testlab
```
fastlane android ui_tests_on_firebase_testlab
```
TestLab testing
### android tests
```
fastlane android tests
```
Runs all the tests
### android build
```
fastlane android build
```
Build
### android firebase
```
fastlane android firebase
```
Upload build to Firebase

----

This README.md is auto-generated and will be re-generated every time [fastlane](https://fastlane.tools) is run.
More information about fastlane can be found on [fastlane.tools](https://fastlane.tools).
The documentation of fastlane can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
